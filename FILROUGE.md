# Fil rouge TP2 - Formation ingénieur Cloud


## Présentation du déroulement

Sur la base d’un cas pratique énoncé ci-après, les participants sont amenés à concevoir mettre en œuvre un environnement technique répondant au besoin

- Réflexion sur l’architecture technique
  - Recensement des socles techniques
  - Définition de l’architecture cible
- Rédaction d’un dossier d’analyse
  - Un dossier d’analyse par équipe
  - Intégrer les diagrammes qui semblent pertinents pour décrire la plateforme cible
- Penser dès le début à la mise œuvre
  - Début de la phase de mise en oeuvre
  - Mise en place de l’architecture technique cible
  - Développement des premiers scripts d’automatisation
- Discussions/Échanges
  - Échanges entre les participants sur cette première expérience
  - Conseils et partage de bonnes pratiques par le formateur

## Contexte 

Une PME Season’up a vu la vente de ses produits exploser à la dernière campagne de Noel. Son site CMS hébergé sur un serveur local n’a pas été capable de tenir la fréquentation associée.

Pour ne plus perdre de nouveaux clients, la société a donc décidé de mettre en place un nouveau CMS sur le cloud.

La solution reposera sur la même architecture technique que le CMS existant, à savoir : WordPress + MySQL, mais avec une contrainte de haute disponibilité.

Pour préparer au mieux ce projet, il a été demandé à l’équipe de production Cloud de mettre en œuvre l’ensemble des environnements de développement et de recette sur le réseau local de la société afin de pouvoir déployer la solution finale dans le cloud.

Le tout devra être géré par un orchestrateur capable de déployer les sources sur chaque environnement de manière automatique.

Vous faites donc partis de cette équipe pour démarrer ce projet sur lequel un niveau de sécurité accru est attendu.

Ultime contrainte imposée par le PDG : la recette doit être accessible depuis n’importe quel endroit, et pas seulement via le réseau interne de l’entreprise.

Pour vous aider, en accord avec le programme, voici ce qui devra être utilisé :

- Environnement Unix / linux
- Mise en œuvre de conteneurs docker
- Séparation front (WordPress) back (MariaDB)
- Utilisation de kubernetes pour la scalabilité
- Utilisation d’Nginx ou Apache pour l’accès externe
- Déploiement sur un opérateur au choix (AWS, Azure ou Google Cloud Platform)

## Point de départ

Ce dépôt GIT ainsi que le dépôt associé https://framagit.org/jpython/kubeadm-ansible permet de rapidement:

- avec Terraform:
  - Créer un VPC sur AWS EC2 (ou fournisseur compatible) 
  - Créer un sous-réseau dans ce VPC
  - Mettre en place les *keypairs* pour administrer l'instance "bastion" ainsi
    que les instances du cluster
  - Créer une instance "bastion"
  - Créer une instance "maître" pour le cluster Kubernetes
  - Créer plusieurs nœud "workers" pour le cluster Kubernetes
  - Ajouter ou enlever des nœuds "workers"
- avec Ansible
  - Déployer et configurer Kubernetes
  - Déployer des applications dans le cluster
  - Ajouter des instances "workers" dans le cluster

Le résultat prévu correspond à ce que nous avons décrit lors du point
d'étape : https://framagit.org/jpython/chti-solution/blob/master/nginx/README.md

À noter que la configuration fonctionne même avec des instances AWS t2.micro et
sans avoir besoin d'IP élastique, sa mise en œuvre ne déclenche donc aucun frais
(à moins de laisser tourner le cluster pendant des semaines...)

L'infrastructure définie pour AWS peut (relativement) facilement être adaptée
à un autre fournisseur IaaS.

## Livrable

À l'issu du TP les livrables sont, comme pour le TP1:

- Le dossier d'analyse sous forme de document
- La documentation de la mise en œuvre à partir de zéro, le redimensionnement
  et la mise au point des divers composants
- Une réflexion libre (mais justifiée) sur les évolutions possibles de la
  plate-forme
- Démonstration de la mise en place et redimensionnement de la plate-forme

Format des livrables: noté sur 4 points (notation totale sur 20):

- 2 points si vous utilisez git et un dépot public (framagit, gitlab, github ou autre)
- 2 points si vous utilisez un format de document *markdown* (comme ce même fichier)


Qualité de la documentation

- 7 points

Pertinence et bonne justification des évolutions suggérées

- 4 points

Qualité des scripts et fichiers de configuration

- 5 points

# Pistes pour l'évolution de la plate forme

- À quoi sert le fichier `terraform.tfstate` dans le répertoire Tf ? 
  Serait-ce une bonne idée de le faire suivre par un système de
  gestion de version tel que GIT, Mercurial, SVN, etc. Dans quels
  cas ? Pourquoi ? 

  > C'est une bonne idée pour que les différents administrateur de la
  > la plateforme ait toujours un état à jour pour pouvoir la manipuler
  > à condition que le dépôt soit accessible uniquement par les
  > administrateurs ! En écriture bien sûr, mais aussi en lecture :
  > il serait dommageable d'exposer publiquement les détails de
  > l'infrastructure. Il ne faut pas oublier surtout de faire 
  > un commit/push à chaque màj du déploiement...
  > Il existe d'autres façon d'enregistrer centralement l'état
  > d'un déploiement, voir la doc de Terraform.

- En interne sur AWS une instance peut récupérer des infos sur
  ses méta-données (comme le script cloud-init passé en user-data)
  en faisant des calls HTTP sur l'IP 169.254.169.254. À quelle
  plage d'adresses appartient cette adresse ? Quel est l'usage
  officiel de ces adresses ? Pourquoi Amazon a fait un tel
  choix à votre avis ?

  > C'est une plage réservé à l'adressage IP automatique sans
  > infrastructure (pas de dns, dhcp, routeur, etc.) norme
  > IETF Apipa/Bonjour/Zéroconf.
  > Le cloud est nécessairement basé sur une infrastructure,
  > il n'y a donc aucune raison d'utiliser ces IPs dans un VPC
  > et le fournisseur souhaite permettre à ses clients de choisir
  > quasiment totalement librement ses adresses de réseau et
  > de machines. Paradoxalement c'est donc un excellent choix
  > même si ça viole une norme de l'Internet Protocol.

- Il y a un problème de sécurité potentiel avec un de nos scripts
  *cloud-init* (i.e. paramètre *user-data* qui contient un script
  exécuté au premier démarrage d'une instance).
  Lequel ? Pourquoi ? Comment pourriez-vous résoudre ce problème ? 

  > Le script de finalisation du bastion contient en clair (enfin
  > base 64, c'est pareil) la clé privé permettant de se connecter
  > aux nœuds du cluster K8s... Au moins il est pas dans le git
  > (quand même !) puisque les clefs sont générées par un script.
  > Du coup la clef est stocké en clair dans l'infra AWS et
  > est accessible à partir de l'instance bastion par une 
  > url http://169.254.169.254/... utilisable par n'importe
  > quel compte utilisateur sur le bastion. Donc s'il est
  > compromis, même pas en root, la clef privée du cluster
  > a fuité... On peut nettoyer le champ user-data des
  > méta-données avec l'API AWS ceci dit.
  > Solution possible : chiffrer la clef avec une phrase
  > de passe à la génération (./generate_keys) et utiliser
  > ssh-agent / ssh-add lors de l'admin du cluster ou bien
  > y copier la clef avec scp plutôt que de la mettre place
  > dans le cloud init. 

- Comment envisageriez-vous de faire évoluer la plate-forme en
  terme d'architecture, de services et logiciels installés ?
  Justifiez votre intérêt pour tout ce que vous suggérez et
  indiquez sommairement comment vous déployeriez ces services.

  > DNS, courrier électronique (SMTP/IMAP), Monitoring
  > du cluster (Prometheus), ELK (Logs), ...
  > Passer de docker à rkt.
  > Passer en IPv6 en interne et externe (double stack
  > externe, simple stack interne), BGP, Multi-AZ.
  > Passer à PostgreSQL au lieu de MariaDB/MySQL.
  > Stockage en cluster CEPH.
  > Cloud privé OpenStack + Cloud public multi-opérateurs.
  > 9p/9fs pour le stockage.
  > Kubernetes sur métal.
  > Mainframe zSerie.
  > Migrations sur serveur RISC-V (quand il seront disponibles) 

- Quelles applications ou services pourriez vous suggérer de
  déplacer en dehors du cluster et du cloud (i.e. sur des serveurs
  internes ou dédiés) ? Pourquoi ?

  > Backup base de donnée, mail (laisser juste un relai dans
  > le cluster, DNS maître

- Dans une logique *devops* comment suggéreriez-vous de déclencher
  le déploiement automatique des logiciels et des éléments de 
  configuration sur une plate-forme de pré-production ?

  > Jenkins ou Gitlab peut piloter le déploiement, il faut
  > juste qu'il soit automatisé à 100% (Bash, Python ou
  > Groovy)

