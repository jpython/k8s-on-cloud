#!/usr/bin/env bash

# APT 
apt -y update
apt -y upgrade
apt -y install squid
apt -y install dirmngr
apt -y install git
cat > /etc/apt/sources.list.d/ansible.list <<EOF
deb http://ppa.launchpad.net/ansible/ansible/ubuntu trusty main
EOF
apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 93C4A3FD7BB9C367
apt -y update
apt -y install ansible
sed --in-place -e '/^http_access deny all/d' /etc/squid/squid.conf
cat >> /etc/squid/squid.conf <<EOF
acl allowedips src 10.0.0.0/24
http_access allow allowedips
http_access deny all
forwarded_for off
cache_dir ufs /var/spool/squid 100 16 256
cache_mem 16 MB
maximum_object_size 15 MB
http_port 3128 transparent
EOF
systemctl restart squid

# Quick and not that dirty
install -d -m 700 /root/.ssh
base64 -d > /root/.ssh/id_rsa <<EOF
PRIVKEY
EOF

install -d -m 700 -u admin -g admin /home/admin/.ssh
base64 -d > /home/admin/.ssh/id_rsa <<EOF
PRIVKEY
EOF

chmod -R u+rwX,go= /root/.ssh
chmod -R u+rwX,go= /home/admin/.ssh
chown -R admin:admin /home/admin/.ssh

cat > /etc/network/interfaces <<EOF
# interfaces(5) file used by ifup(8) and ifdown(8)
# Include files from /etc/network/interfaces.d:
source-directory /etc/network/interfaces.d
auto lo
iface lo inet loopback
auto eth0
iface eth0 inet dhcp
  post-up sysctl net.ipv4.ip_forward=1
  post-up iptables -A POSTROUTING -t nat -s 10.0.0.0/24 -j MASQUERADE
allow-hotplug eth0

iface eth0 inet6 manual
  up /usr/local/sbin/inet6-ifup-helper
  down /usr/local/sbin/inet6-ifup-helper
EOF

cat > /home/admin/check_node_ci <<'EOF'
#!/usr/bin/env bash

for i in $*
do
  echo -en "Node : 10.0.0.${i}\t"
  ssh -o StrictHostKeyChecking=no 10.0.0.$i "ls /tmp/cloud-init* 2> /dev/null" 2> /dev/null
  echo
done
EOF

chown admin:admin /home/admin/check_node_ci
chmod +x admin:admin /home/admin/check_node_ci

date > /tmp/cloud-init-bastion-ok
